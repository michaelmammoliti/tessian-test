# Tessian Test

### Time
- 5 hours circa

## What is missing
- tests using `enzyme` and `jest`;
- types using `flow` (I've added prop-types);
- responsive Design;
- CSS styleguide: reusable UI colors and palettes;
- comments in the code;
- a calendar for one filer for the query.

## Comments
- I could have implemented a dynamic form to get the values and create the query. (organisation and repository)
- As you can see I am used to organise my reducer quiet differently as I think the logic should be handled inside the View or the Actions and not in the Reducer. This allows me to do have a better way to access data from the state (through mapStateToProps in the connect()) and to have `getState()-free` thunked actions. Quality and cleaning.
- I'm using webpack autoresolver for `main.js` files;
- I'm using a fetch decorator, you can find it in `client/app/utilities/fetch.js`;

## How To
Simplified:

```
git clone https://github.com/MichaelMammoliti/tessian-test.git && cd tessian-test && npm i && npm start
```

## Environment
- Node: `10.2.1`
- npm: `6.1.0`
