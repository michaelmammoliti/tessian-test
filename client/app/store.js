import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';

// Reducers
import issuesListReducer from './containers/issues-list/state/reducer';
import issuesFiltersReducer from './containers/issues-filters/state/reducer';

const reducerCollection = combineReducers({
  issuesList: issuesListReducer,
  issuesFilters: issuesFiltersReducer,
});

export default createStore(reducerCollection, {}, applyMiddleware(thunk, logger));
