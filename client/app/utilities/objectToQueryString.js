const objectToQueryString = obj => {
  const values = [];

  const reducerFn = (acc, key) => {
    const value = obj[key];
    const keyValue = `${key}=${value}`;

    if (!value) {
      return acc;
    }

    values.push({ value, key });

    return (values.length === 1)
      ? `?${keyValue}`
      : `${acc}&${keyValue}`
    ;
  };

  return Object.keys(obj)
    .reduce(reducerFn, '')
  ;
};

export default objectToQueryString;
