import objectToQueryString from './objectToQueryString.js';

const fetchDecorator = (url, options) => {
  let newUrl = url;
  const newOptions = { ...options };

  delete newOptions.queryParameters;

  if (options.queryParameters) {
    const queryString = objectToQueryString(options.queryParameters);

    newUrl = `${url}${queryString}`;
  }

  return window.fetch(newUrl, newOptions)
    .then(response => response.json())
  ;
};

export default fetchDecorator;
