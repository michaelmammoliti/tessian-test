import 'whatwg-fetch';

import { Provider } from 'react-redux';
import store from './store';

import IssuesPage from './pages/issues';

import styles from './app.scss'; // eslint-disable-line

const App = () => (
  <Provider store={store}>
    <IssuesPage />
  </Provider>
);

export default App;
