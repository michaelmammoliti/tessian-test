import CONSTANTS from './constants';
import IssuesListActions from '../../issues-list/state/actions';

const changeFilter = ({ name, value, filters }) => dispatch => {
  const newFilters = {
    ...filters,
    [name]: value,
  };

  dispatch({
    type: CONSTANTS.CHANGE_FILTER,
    payload: {
      filters: newFilters,
    },
  });

  dispatch(IssuesListActions.fetchIssues(newFilters));
};


export default {
  changeFilter,
};
