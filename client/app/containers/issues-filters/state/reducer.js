import CONSTANTS from './constants';

const initialState = {
  filters: {},
};

const PullRequestsReducer = (state = initialState, action = {}) => {
  const { type, payload } = action;
  let newState;

  switch (type) {
    case CONSTANTS.CHANGE_FILTER:
      newState = { ...state, ...payload };
      break;

    default:
      newState = state;
      break;
  }

  return newState;
};

export default PullRequestsReducer;

