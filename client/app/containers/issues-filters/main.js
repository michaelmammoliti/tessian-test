import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import FormField from '../../components/form-field';

import IssuesFiltersActions from './state/actions';
import IssuesFiltersMocks from './mocks';

import styles from './main.scss';

const IssuesFiltersContainer = ({ filters, actions }) => {
  // Events
  // ================================================
  const handleFilterChange = ({ value, name }) => {
    actions.changeFilter({ name, value, filters });
  };

  // Builders
  // ================================================
  const buildItems = (filterItem, index) => {
    const callbacks = {
      onChange: handleFilterChange,
    };

    return (
      <div className={styles['issues-filters--item']} key={index}>
        <FormField {...filterItem} {...callbacks} />
      </div>
    );
  };

  // Render
  // ================================================
  return (
    <div className={styles['issues-filters']}>
      {IssuesFiltersMocks.headerFilters.map(buildItems)}
    </div>
  );
};

IssuesFiltersContainer.displayName = 'IssuesFiltersContainer';

IssuesFiltersContainer.propTypes = {
  filters: PropTypes.object, // eslint-disable-line
  actions: PropTypes.object, // eslint-disable-line
};

const mapStateToProps = ({ issuesFilters }) => ({
  ...issuesFilters,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(IssuesFiltersActions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(IssuesFiltersContainer);
