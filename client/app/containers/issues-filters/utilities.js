const generateItems = (itemCollection, selected) => {
  const mapperFn = (item, index) => ({
    value: item,
    text: item,
    selected: item === selected,
    id: index,
  });

  return itemCollection.map(mapperFn);
};

export default {
  generateItems,
};
