import IssueFiltersUtilities from './utilities';

const headerFilters = [
  {
    uiElement: 'dropdown',
    name: 'order',
    label: 'order',
    placeholder: 'order',
    items: IssueFiltersUtilities.generateItems(['assigned', 'created', 'mentioned', 'subscribed', 'all']),
  },
  {
    uiElement: 'dropdown',
    name: 'state',
    label: 'state',
    placeholder: 'state',
    items: IssueFiltersUtilities.generateItems(['add', 'open', 'closed']),
  },
  {
    uiElement: 'dropdown',
    name: 'direction',
    label: 'direction',
    placeholder: 'direction',
    items: IssueFiltersUtilities.generateItems(['asc', 'desc']),
  },
  {
    uiElement: 'input',
    type: 'text',
    name: 'mentioned',
    label: 'mentioned',
    placeholder: 'mentioned',
  },
  {
    uiElement: 'input',
    type: 'text',
    name: 'labels',
    label: 'labels',
    placeholder: 'labels',
  },
  {
    uiElement: 'input',
    type: 'text',
    name: 'milestone',
    label: 'milestone',
    placeholder: 'milestone',
  },
  {
    uiElement: 'input',
    type: 'text',
    name: 'assignee',
    label: 'assignee',
    placeholder: 'assignee',
  },
  {
    uiElement: 'input',
    type: 'text',
    name: 'creator',
    label: 'creator',
    placeholder: 'creator',
  },
  // This has been commented as I am not including a calendar.
  // {
  //   uiElement: 'calendar',
  //   name: 'since',
  //   label: 'since',
  //   placeholder: 'since',
  // },
];

export default {
  headerFilters,
};
