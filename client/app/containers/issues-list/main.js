import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styles from './main.scss';
import IssuesActions from './state/actions';

import Issues from '../../components/issues';

class IssuesListContainer extends React.Component {
  // Lifecycle
  // ================================================
  componentDidMount() {
    const { organisation, repository } = this.props;

    this.props.actions.fetchIssues({
      organisation,
      repository,
    });
  }

  // Render
  // ================================================
  render() {
    const { fetchIssuesListRequestStatus, items } = this.props;

    return (
      <div className={styles['issues-list']}>

        {fetchIssuesListRequestStatus === 'fail' &&
          <div className={styles['issues-list--fail']}>
            <span>error while loading data!</span>
          </div>
        }

        {fetchIssuesListRequestStatus === 'pending' &&
          <div className={styles['issues-list--pending']}>
            <span>Wait.. I will be right back with some data :D</span>
          </div>
        }

        {(fetchIssuesListRequestStatus === 'success' && !items.length) &&
          <div className={styles['issues-list--success-empty']}>
            <span>Oops.. No issues matching your filters.</span>
          </div>
        }

        {(fetchIssuesListRequestStatus === 'success' && items.length) &&
          <div className={styles['issues-list--success']}>
            <Issues items={items} />
          </div>
        }

      </div>
    );
  }
}

IssuesListContainer.displayName = 'IssuesListContainer';

IssuesListContainer.propTypes = {
  fetchIssuesListRequestStatus: PropTypes.oneOf(['unsent', 'fail', 'pending', 'success']),
  items: PropTypes.arrayOf(PropTypes.object),
  actions: PropTypes.shape({
    fetchIssues: PropTypes.func,
    changeFilter: PropTypes.func,
  }),
  organisation: PropTypes.string,
  repository: PropTypes.string,
};

const mapStateToProps = ({ issuesList }) => ({
  ...issuesList,

  // @param: organisation
  // @param: repository
  // =============================================
  // we could pass to these via mapStateToProps and have a reducer/UI
  // which provides these values dynamically.
  organisation: 'atom',
  repository: 'atom',
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(IssuesActions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(IssuesListContainer);
