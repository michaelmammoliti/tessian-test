import CONSTANTS from './constants';

const initialState = {
  items: [],
  fetchIssuesListRequestStatus: undefined,
};

const PullRequestsReducer = (state = initialState, action = {}) => {
  const { type, payload } = action;
  let newState;

  switch (type) {
    case CONSTANTS.FETCH_ISSUES:
    case CONSTANTS.FETCH_ISSUES_PENDING:
    case CONSTANTS.FETCH_ISSUES_SUCCESS:
    case CONSTANTS.FETCH_ISSUES_FAIL:
      newState = { ...state, ...payload };
      break;

    default:
      newState = state;
      break;
  }

  return newState;
};

export default PullRequestsReducer;

