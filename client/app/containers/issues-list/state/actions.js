import CONSTANTS from './constants';
import IssuesListApi from './api';

const fetchIssuesListSuccess = ({ items }) => ({
  type: CONSTANTS.FETCH_ISSUES_SUCCESS,
  payload: {
    fetchIssuesListRequestStatus: 'success',
    items,
  },
});

const fetchIssuesListPending = () => ({
  type: CONSTANTS.FETCH_ISSUES_PENDING,
  payload: {
    fetchIssuesListRequestStatus: 'pending',
  },
});

const fetchIssuesFail = () => ({
  type: CONSTANTS.FETCH_ISSUES_FAIL,
  payload: {
    fetchIssuesListRequestStatus: 'fail',
  },
});

const fetchIssues = payload => dispatch => {
  dispatch(fetchIssuesListPending());

  const success = items => {
    dispatch(fetchIssuesListSuccess({
      items,
    }));
  };

  const fail = () => {
    dispatch(fetchIssuesFail());
  };

  IssuesListApi.fetchIssuesList(payload)
    .then(success)
    .catch(fail)
  ;
};

const changeFilter = ({ value, name, filters }) => dispatch => {
  const newFilters = {
    ...filters,
    [name]: value,
  };

  dispatch(fetchIssues(newFilters));

  dispatch({
    type: CONSTANTS.CHANGE_FILTER,
    payload: {
      filters: newFilters,
    },
  });
};


export default {
  fetchIssues,
  changeFilter,
};
