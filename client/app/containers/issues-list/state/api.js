const fetchIssuesList = ({ filters, organisation, repository }) =>
  fetch(`https://api.github.com/repos/${organisation}/${repository}/issues`, {
    method: 'GET',
    queryParameters: filters,
  })
;

export default {
  fetchIssuesList,
};
