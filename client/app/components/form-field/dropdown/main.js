import styles from './main.scss';

class Dropdown extends React.Component {
  constructor(props) {
    super(props);

    // State
    this.state = {
      selectedItemIndex: this.getSelectedItemIndex(),
    };

    // Event Binding
    this.handleChange = this.handleChange.bind(this);
  }

  // Getters
  // =============================================
  getSelectedItemIndex() {
    const { items, selectedIndex } = this.props;

    if (!items || !items.length) {
      return 0;
    }

    const newIndex = (typeof selectedIndex === 'undefined')
      ? items.findIndex(item => item.selected)
      : selectedIndex
    ;

    return (newIndex !== -1)
      ? newIndex
      : 0
    ;
  }

  // Getters
  // =============================================
  handleChange(event) {
    const { items, onChange, name } = this.props;
    const { value } = event.target;
    const itemIndex = items.findIndex(item => item.value === value);

    this.setState({
      selectedItemIndex: itemIndex,
    });

    onChange({
      name,
      value,
      index: itemIndex,
    });
  }

  // Render
  // =============================================
  render() {
    const { name, items, label } = this.props;
    const { selectedItemIndex } = this.state;

    if (!items || !items.length) {
      return null;
    }

    return (
      <div className={styles['dropdown']}>
        <div className={styles['dropdown--label']}>
          <span>{label}</span>
        </div>

        <div className={styles['dropdown--input']}>
          <select
            name={name}
            defaultValue={items[selectedItemIndex].value || ''}
            onChange={this.handleChange}
          >
            {items.map((item, index) => (
              <option key={index} value={item.value}>{item.text}</option>
            ))}
          </select>
        </div>
      </div>
    );
  }
}

Dropdown.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    selected: PropTypes.bool,
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    text: PropTypes.string,
    id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  })),
  label: PropTypes.string,
  name: PropTypes.string,
  selectedIndex: PropTypes.number,
  onChange: PropTypes.func,
};

Dropdown.defaultProps = {
  onChange: () => {},
};

export default Dropdown;
