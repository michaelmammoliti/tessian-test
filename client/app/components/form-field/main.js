import Dropdown from './dropdown';
import Input from './input';

const availableFields = {
  dropdown: Dropdown,
  input: Input,
};

const FormField = props => {
  const Comp = availableFields[props.uiElement];

  if (!Comp) {
    return null;
  }

  return <Comp {...props} />;
};

FormField.propTypes = {
  uiElement: PropTypes.string,
  type: PropTypes.string,
  items: PropTypes.arrayOf(PropTypes.shape({})),
  value: PropTypes.string,
  name: PropTypes.string,
  label: PropTypes.string,
};

export default FormField;
