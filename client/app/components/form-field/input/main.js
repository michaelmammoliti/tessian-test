import styles from './main.scss';

class Input extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: props.value,
    };

    this.handleChange = this.handleChange.bind(this);

    this.changeTimeout = undefined;
  }

  // Events
  // =============================================
  handleChange(event) {
    const { value } = event.target;
    const { name, onChange } = this.props;

    this.setState({
      value,
    });

    // Debouncing
    window.clearTimeout(this.changeTimeout);

    this.changeTimeout = window.setTimeout(() => {
      onChange({ name, value });
    }, 500);
  }

  // Render
  // =============================================
  render() {
    const {
      label,
      placeholder,
      name,
      type,
    } = this.props;

    const { value } = this.state;

    return (
      <div className={styles['input']}>
        <div className={styles['input--label']}>
          <span>{label}</span>
        </div>

        <div className={styles['input--element']}>
          <input
            type={type}
            name={name}
            value={value}
            placeholder={placeholder}
            onChange={this.handleChange}
          />
        </div>
      </div>
    );
  }
}

Input.displayName = 'Input';

Input.defaultProps = {
  value: '',
  type: 'text',
  onChange: () => {},
  onBlur: () => {},
};

Input.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string,
  type: PropTypes.string,
  value: PropTypes.string,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
};

export default Input;
