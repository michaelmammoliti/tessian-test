import IssuesItem from './issues-item';

import styles from './main.scss';

const Issues = ({ items }) => {
  if (!items || !items.length) {
    return null;
  }

  // Render
  // ======================================================
  return (
    <div className={styles['issues']}>
      {items.map(item => (
        <IssuesItem key={item.id} {...item} />
      ))}
    </div>
  );
};

Issues.displayName = 'Issues';

Issues.defaultProps = {
  items: [],
};

Issues.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({})),
};

export default Issues;
