import styles from './main.scss';

const IssuesItem = (props) => {
  const {
    title,
    user,
    state,
  } = props;

  return (
    <div className={styles['issue-item']}>
      <div className={styles['issue-item--title']}>
        <h2>{title}</h2>
      </div>

      <div className={styles['issue-item--status']}>
        <span>{state}</span>
      </div>

      <div className={styles['issue-item--author']}>
        <span>{user.login}</span>
      </div>

    </div>
  );
};

IssuesItem.displayName = 'IssuesItem';
IssuesItem.propTypes = {
  title: PropTypes.string,
  state: PropTypes.string,
  user: PropTypes.shape({}),
};

export default IssuesItem;
