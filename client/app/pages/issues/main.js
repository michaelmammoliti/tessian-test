import IssuesList from '../../containers/issues-list';
import IssuesFilters from '../../containers/issues-filters';

import styles from './main.scss';

const IssuesPage = () => (
  <div className={styles['issues-page']}>
    <div className={styles['issues-page--header']}>
      <IssuesFilters />
    </div>
    <div className={styles['issues-page--body']}>
      <IssuesList />
    </div>
  </div>
);

export default IssuesPage;
